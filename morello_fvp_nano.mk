# Copyright (c) 2020 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause

$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit_only.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/updatable_apex.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/minimal_system.mk)

PRODUCT_NAME := morello_fvp_nano
PRODUCT_BRAND := Android
PRODUCT_MANUFACTURER := Arm
PRODUCT_DEVICE := morello
PRODUCT_MODEL := Morello Fast Model (Nano build)

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/init.morello_fvp_nano.rc:root/init.morello.rc

# https://source.android.com/devices/tech/ota/dynamic_partitions/implement
# https://source.android.com/devices/tech/ota/dynamic_partitions/implement#adb-remount
PRODUCT_USE_DYNAMIC_PARTITIONS := false

# Don't use the precollected boot image profile for generating boot image
PRODUCT_USE_PROFILE_FOR_BOOT_IMAGE := false

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/fstab.morello:ramdisk/fstab.morello \
    $(LOCAL_PATH)/fstab.morello:root/fstab.morello \

PRODUCT_PACKAGES += \
    com.android.runtime \
    init_vendor \
    ip \
    selinux_policy_nonsystem \

# ease debugging/troubleshooting
PRODUCT_PACKAGES += \
    gdbserver \
    ping \

# FVP-specific tools
PRODUCT_PACKAGES += \
    toggle_mti
