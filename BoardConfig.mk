# Copyright (c) 2020 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause

TARGET_NO_BOOTLOADER := true
TARGET_NO_KERNEL := true
TARGET_NO_RECOVERY := true

TARGET_ARCH := arm64
TARGET_ARCH_VARIANT := armv8-2a_with_caps
TARGET_CPU_VARIANT := generic
TARGET_CPU_ABI := arm64-v8a

TARGET_2ND_ARCH := morello
TARGET_2ND_CPU_VARIANT := generic
TARGET_2ND_CPU_ABI := arm64-v8a

include $(dir $(lastword $(MAKEFILE_LIST)))/BoardConfig-partitions.mk
