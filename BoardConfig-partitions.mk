# Copyright (c) 2020 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause

TARGET_USERIMAGES_USE_EXT4 := true
# We need normal images, not sparse images.
TARGET_USERIMAGES_SPARSE_EXT_DISABLED := true

# See https://source.android.com/devices/bootloader/system-as-root
BOARD_BUILD_SYSTEM_ROOT_IMAGE := false
BOARD_USES_RECOVERY_AS_BOOT := false

# Workaround: these lines are just to please fs_config_generator.py
# which is passed "--all-partitions $(fs_config_generate_extra_partition_list)".
# The variable would be empty otherwise triggering an argument parsing error.
# OEMIMAGE has no other side-effects in the build system, hope it stays so.
# See build/make/tools/fs_config/Android.mk for details.
BOARD_USES_OEMIMAGE := true
BOARD_OEMIMAGE_FILE_SYSTEM_TYPE := ext4

BOARD_SYSTEMIMAGE_PARTITION_SIZE := 167772160    # 160 MB
BOARD_USERDATAIMAGE_PARTITION_SIZE := 536870912  # 512 MB
