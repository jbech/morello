# Copyright (c) 2020 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/morello_fvp_nano.mk \

COMMON_LUNCH_CHOICES := \
    morello_fvp_nano-eng \
